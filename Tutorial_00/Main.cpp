
#include<iostream>;
#include<string>;

using namespace std;

int main()
{
	cout << "Commencons par saisir du texte" << endl;

	string texte("");

	getline(cin, texte);

	cout << "Vous avez saisi : " << texte << endl;

	cout << "Hello world !!!" << endl;

	string bienvenue("Vous allez coder une calculatrice !");

	cout << "Saisissez le premier nombre entier :" << endl;

	int nombre1(0);

	cin >> nombre1;

	cout << "Saisissez le second nombre entier : " << endl;

	int nombre2(0);

	cin >> nombre2;

	int resultat(nombre1 + nombre2);

	cout << "Le resultat de l'addition est : " << endl << nombre1 << " + " << nombre2 << " = " << resultat << endl;

	cin.ignore();

	cout << "Et maintenant, affichons du texte !" << endl;

	string nomEtPrenom("");

	cout << "Saisissez vos noms et prenoms :" << endl;

	getline(cin, nomEtPrenom);

	cout << "Vous vous appelez : " << nomEtPrenom << endl;

	system("PAUSE");

	return 0;
}
